// import of the classes needed for the CRC rule as well as the classes nedede for the bootstrap components
import {Row, Col, Card} from 'react-bootstrap' 

export default function Highlights(){
	
	return(
		<Row className = "my-3">
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				       		<h2>Learn From Home</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo nulla, tristique nec sodales non, cursus sit amet lacus. In hac habitasse platea dictumst. Praesent pellentesque pharetra rutrum. Sed lacinia posuere sem at bibendum. Maecenas commodo ultrices eros. Curabitur sed volutpat lacus.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				       		<h2>Study Now, Pay Later</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo nulla, tristique nec sodales non, cursus sit amet lacus. In hac habitasse platea dictumst. Praesent pellentesque pharetra rutrum. Sed lacinia posuere sem at bibendum. Maecenas commodo ultrices eros. Curabitur sed volutpat lacus.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3">
				      <Card.Body>
				        <Card.Title>
				       		<h2>Be part of our community</h2>
				        </Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum leo nulla, tristique nec sodales non, cursus sit amet lacus. In hac habitasse platea dictumst. Praesent pellentesque pharetra rutrum. Sed lacinia posuere sem at bibendum. Maecenas commodo ultrices eros. Curabitur sed volutpat lacus.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>

		)
}