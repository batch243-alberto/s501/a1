import {useState, useEffect, useContext} from 'react';
import {Col, Button, Card} from 'react-bootstrap';
import UserContext from '../UserContext';

import {Link} from 'react-router-dom';

// Use the state hook for this component to be able to store its state specifically to monitor the number of enrollees
// States are used to keep track information related to individual components
// initialize the value of the getter 
// Syntax: 
	// const [getter, setter] = useState(initialGetterValue)
	// getter is variable, setter is a function


	// since enrolles is declare as a constant variable, directly reassigning the claus is not allowed or will cause an error
	// enrollees = 1;



export default function Cards({courseProp}) {

	const{_id, name, description, price, slots} = courseProp;

	const [enrollees, setEnrollees] = useState(0);
	const [slotsAvailable, setSlotsAvailable] = useState(slots)
	const [isAvailable, setIsAvailable] = useState(true);
	const {user} = useContext(UserContext);

	// Add a "useEffect" hook to have a "CourseCard" component to perform a certain task after every DOM update
		// Syntax: useEffect(functionToBeTriggered, [statesToBeMonitored])

	useEffect(() => {
		if(!slotsAvailable){
			setIsAvailable(false);
		}
	}, [slotsAvailable]);


	function enroll(){
		
		setEnrollees(enrollees+1)
		setSlotsAvailable(slotsAvailable-1)
		if(slotsAvailable === 1){

			alert("Congratulations, you were able to enroll before the cut!");
		}
		
	} 

	return (
			<Col xs={12} md = {4}>
				<Card className = "cardCase p-3">
				    <Card.Body>
				      <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>
					        Description:
					        </Card.Subtitle>
					        <Card.Text>
					          {description}   
					        </Card.Text>
					         <Card.Subtitle>
					        	Price:
					        </Card.Subtitle>
					        <Card.Text>  
					        P {price} 
					        </Card.Text>

					         <Card.Subtitle>
					        	Enrollees:
					        </Card.Subtitle>
					        <Card.Text>  
					        {enrollees}
					        </Card.Text>

					         <Card.Subtitle>
					        	Slots:
					        </Card.Subtitle>
					        <Card.Text>  
					        {slotsAvailable} slots 
					        </Card.Text>
					        {
					        	(user !==null) ?
					        <Button as = {Link} to = {`/courses/${_id}`}
					        	variant="primary" disabled = {!isAvailable}>Details</Button> : 
					         <Button 
					        	as = {Link} 
					        	to ="/login" 
					        	variant="primary" onClick = {enroll} disabled = {!isAvailable}>Details</Button>

					        }
				    	
					</Card.Body>
				</Card>
			</Col>	
		)
}

