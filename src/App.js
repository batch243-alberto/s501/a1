// import {Fragment} from 'react';
// require('dotenv').config()
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


import AppNavbar from './components/AppNavbar';
import Register from './pages/Register';
import Home from './pages/Home';
import Courses from './pages/Courses';
import LogIn from './pages/LogIn';
import LogOut from './pages/LogOut';
import Error from './pages/Error';
import CourseView from './pages/CourseView';

import {UserProvider} from './UserContext';
import {useState, useEffect} from 'react';



function App() {

  // State hook for the user that will be globaly accessible using the useContext
  // This will also be used to store the user information and be used for validating if a user is logged in on the app or not 
  // Allows us to consume the User context object and its properties to use for user value

const[user, setUser] = useState({id: null, isAdmin:false});

// Function for clearing local storage 
const unSetUser = () => {
  localStorage.removeItem("email")
  // setUser(null);
}

useEffect(() => {
  console.log(user);
}, [user])
 
useEffect(() =>{
  fetch(`${process.env.REACT_APP_URI}/users/profile`, 
    {headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }}).then(res => res.json())
      .then(data => {
    console.log(data);

    setUser({id: data._id, isAdmin: data.isAdmin});
  })
}, [])

  return (
    // Router/BrowseRouter > Routes > Route
    <UserProvider value = {{user, setUser, unSetUser}}>
      <Router>
        <AppNavbar/>
        <Routes>
          <Route  path ="/" element = {<Home/>}/>
          <Route  path ="/courses" element = {<Courses/>}/>
          <Route  path ="/courses/:courseId" element = {<CourseView/>}/>
          <Route  path ="/login" element = {<LogIn/>}/>
          <Route  path ="/register" element = {<Register/>}/>
          <Route  path ="/logout" element = {<LogOut/>}/>
          <Route  path ="*" element = {<Error/>}/>
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
