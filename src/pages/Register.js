import {Button, Form, Row, Col, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import React from 'react'

import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){
	// State hooks to store the values of the input field from our user
	const [firstName, setfirstName] = useState('');
	const [lastName, setlastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setmobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	 const navigate = useNavigate();


	// const {user, setUser} = useContext(UserContext);

	/*Business Logic*/
	// We want to disable the register button if one of the input fields is empty

	useEffect(() =>{
		firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "" && password1 === password2
		        ? setIsActive(true)
		        : setIsActive(false)


	}, [firstName, lastName, email, mobileNo, password1, password2])

	// this function will be triggered when the inputs in the Form will be submitted

	function registerUser(event){
		event.preventDefault();


		// alert(`Congratulations ${email}, you are now registered in our website!` );

		// localStorage.setItem("email", email);
		// setUser(localStorage.getItem("email"));
		// setEmail('');
		// setPassword1('');
		// setPassword2('');

		fetch(`${process.env.REACT_APP_URI}/users/register`, {
			method:"POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password1,
                password2: password2
			})
		}).then(res => res.json())
				.then(data => {
					console.log(data)

				if(data.emailExists){
					Swal.fire({
					    title: "Email Already Exists",
					    icon: "error",
					    text: "Please use another email"
					})

					setPassword1('');
					setPassword2('');
				}
				else if(!data.mobileStatus){
					Swal.fire({
					    title: "MobileNo should be 11 digits or higher",
					    icon: "error",
					    text: "Please provider another mobile digits."
					})

					setPassword1('');
					setPassword2('');
				}
				else{
					Swal.fire({
					    title: "Registration Successful",
					    icon: "success",
					    text: "You may now login to the website!"
					})

					navigate('/login');
				}

				})


	}

	return(

		// (user!== null) ?
		// <Navigate to = "*"/>
		// :
		// (user.id) ? 
		// <Navigate to = "/"/>
		// :
		<Container>
		<Row className='w-100'>
			<Col className ="col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {registerUser} className ="bg-secondary p-3">

						<Form.Group className="mb-3" controlId="firstName">
						  <Form.Label>firstName</Form.Label>
						  <Form.Control 
						  	type="firstName" 
						  	placeholder="firstName" 
						  	value ={firstName} 
						  	onChange = {event => setfirstName(event.target.value)}
						  	required />
						</Form.Group>

						<Form.Group className="mb-3" controlId="lastName">
						  <Form.Label>lastName</Form.Label>
						  <Form.Control 
						  	type="lastName" 
						  	placeholder="lastName" 
						  	value ={lastName} 
						  	onChange = {event => setlastName(event.target.value)}
						  	required />
						</Form.Group>


				      <Form.Group className="mb-3" controlId="email">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="email" 
				        	value ={email} 
				        	onChange = {event => setEmail(event.target.value)}
				        	required />
				        <Form.Text className="text-muted">
				          We'll never share your email with anyone else.
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="mobileNo">
				        <Form.Label>mobileNo</Form.Label>
				        <Form.Control 
				        	type="mobileNo" 
				        	placeholder="mobileNo" 
				        	value ={mobileNo} 
				        	onChange = {event => setmobileNo(event.target.value)}
				        	required />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Enter your desired password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password1} 
				        	onChange = {event => setPassword1(event.target.value)}
				        	required/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password2">
				        <Form.Label>Verify password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	required />
				      </Form.Group>
				     
				      <Button variant="primary" type="submit" disabled = {!isActive}>
				        Register
				      </Button>

				    </Form>
			</Col>
		</Row>
	</Container>
		)
}