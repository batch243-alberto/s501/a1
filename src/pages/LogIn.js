import {Button, Form, Row, Col, Container} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
// importing sweetalert2
import Swal from 'sweetalert2';


export default function LogIn(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false)

	// const[user, setUser] = useState(null);

	const {user, setUser} = useContext(UserContext);

	useEffect(() =>{
		email !== "" && password !== "" ?
		setIsActive(true) : setIsActive(false)

	}, [email, password])


	const authenticate = (event) => {
		event.preventDefault()

		// alert(`You are now logged in!` );
		// Set the email of the authenticated user in the local storage
		// Syntax
			// localStorage.setItem('propertyName', value)

		// Storing information in the local storage will make the data persistent even as the page is refreshed unlike with the use of states where informations is reset when refreshing the page
		// localStorage.setItem('email', email);

		// Set the global user state to have properties obtained from the local storage

		// Through access to the user information can be done vua the localStorage this is necessary to update the user state which will help update the App component and rerend it to avoid refreshing the page upon user login and logout
/*		setUser(localStorage.getItem("email"));

		setEmail('');
		setPassword('');
*/
		// Process wherein it will fetch a request to the corresponding API
		// The header information "Content-type" is used to specify that the information being sent to the backend will be sent in the form of json 
		// the fetech request will communicate with our backend application providing it with a stringified JSON

		// Syntax:
			// fetch('url', {options: method, headers body})
			// .then(res => res.json())
			// .then(data => {data.process})
		fetch(`${process.env.REACT_APP_URI}/users/login`, {
			method:"POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		}).then(res => res.json())
		.then(data => {
			// it is good to practice print out the result of our fetch request to ensure the correct information is received in out frontend application
			console.log(data);

			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Check your lodin details and try again."
				});
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Welcome to our website!"
				});;
				setPassword('');
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_URI}/users/profile`, 
				{headers: {
					Authorization: `Bearer ${token}`
				}}).then(res => res.json())
					.then(data => {
				console.log(data);
				setUser({id: data._id, isAdmin: data.isAdmin});
			})
		}

	}

	return (
		(user.id !== null ) ? 
			<Navigate to = "/" />
			:
		<Container>
			<Row>
				<Col className = "col-md-4 col-8 offset-md-4 offset-2 mt-3">
				   <Form onSubmit = {authenticate} className ="bg-secondary p-3">
				     <Form.Group className="mb-3" controlId="email">
				       <Form.Label>Email address</Form.Label>
				       <Form.Control 
				       		type="email" 
				       		placeholder="Enter email"
				       		value = {email}
				       		onChange = {event => setEmail(event.target.value)}
				       		required />
				       <Form.Text className="text-muted">
				         We'll never share your email with anyone else.
				       </Form.Text>
				     </Form.Group>

				     <Form.Group className="mb-3" controlId="password">
				       <Form.Label>Password</Form.Label>
				       <Form.Control 
				       		type="password" 
				       		placeholder="Password"
				       		value = {password}
				       		onChange = {event => setPassword(event.target.value)}
				       		required />
				     </Form.Group>
				     <Button variant="primary" type="submit" disabled = {!isActive}>
				       Submit
				     </Button>
				   </Form>
				</Col>
			</Row>
		</Container>
	 );
}