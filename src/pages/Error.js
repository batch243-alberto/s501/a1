import {Container, Col, Row, Button} from 'react-bootstrap';
// import {NavLink} from 'react-router-dom';


export default function Error(){
	return(
		<Container>
			<Row className ="text-center">
				<Col>
					<h2>404 error</h2>
					<p>This page does not exist</p>
					<Button href="/" type="submit">Go back to home page</Button>{' '}					 
				</Col>
			</Row>
		</Container>
		)
}