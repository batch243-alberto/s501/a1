import Cards from '../components/Cards'
// import coursesData from '../data/courses.js'
import {Container, Row} from 'react-bootstrap'
import {useEffect, useState} from 'react';

export default function Courses(){
	const [courses, setCourses] = useState([]);

	// const local = localStorage.getItem("email");

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`).then(res => res.json())
			.then(data => {
				console.log(data);

				// Sets the "courses" state to map the data retrieved from te fetch request into several "CourseCard" Component
				setCourses(data.map((course) => {
					return(
				<Cards key = {course._id} courseProp = {course}/>
					)
				}))
			})
	}, [])

	return (
		// Syntax: 
			// localStorage.getItem("propertyName")

		<Container >
			<Row className = "my-3">
				{courses}
				
			</Row>
		</Container>
		)
}