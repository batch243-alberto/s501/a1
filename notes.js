// Syntax: npx create-react-app "project-name"

// After creating project execute the command "npm start" to run our project in our localhost

// We delete the unnecessary files from the newly created project 
	// 1. App.test.js
	// 2. Index.css
	// 3. logo.svg
	// reportWebVitals.js

// After we deleted the unnecessary files, we encountered errors 
	// errors encountered: importation of the deleted files  
	// to fix we remove the syntax or codes of the imported files

// Now we have a blank state where we can start building our own ReactJS app.

// Important Note: 
	// Similar to NodeJS and expressJS, we can install packages in our react application to make the work easier for us 
		// example: npm install react-bootstrap bootstrap

	// The "import" statement allows us to use the code/exported modules from files similar to how we use the require keyword in NodeJS

	// ReactJS components are independent, reusable pieces of code which normally contains Javascript and JSX syntax which make up a part of our application. 
	// JSX combination of js and html 


// [Section] Props 
/*properties is an information that a component receives, usually from a parent component*/
// one can use the same component and feed different data to the component for rendering

// key is for uniq indentifier

/*
const courses = courseData.map(course => {
	return(
		<CourseCard courseProp = {course} />
	)
})
courseData.map(course => <div key={coruse.id}>course.name</div>)*/
// ({courseProp}) access to the object variable 

// path = "*" s own 404 na page

// Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the value

// All information provided to the Provider component can be accessed later on from the context object as properties